#include <bits/stdc++.h>

using namespace std;

//Konversi string to integer
int strToInt(string s){
  int val = 0;
  int i = 0;
  while(s[i] <= '9' && s[i] >= '0'){
    val = val*10 + (s[i] - '0');
    i++;
  }
  return val;
}

//penukaran data
int swap(int& a, int& b){
  int temp = a;
  a = b;
  b = temp;
}

//implementasi Insertion Sort
void InsertionSort(int* T, int n){
  for (int i = 2; i <= n; i++){
    int j = i;
    while( j > 0 && T[j-1] > T[j]){
      swap(T[j],T[j-1]);
      j--;
    }
  }
}

//implementasi Selection Sort
void SelectionSort(int *T, int n){
  for (int i = 1; i < n; i++){
    int idxMin = i;
    for (int j = i+1 ; j <= n; j++){
      if (T[j] < T[idxMin])
        idxMin = j;
    }
    if (idxMin != i)
      swap(T[i],T[idxMin]);
  }
}


void Merge(int *T, int kiri, int tengah, int kanan){
  // int Temp[kanan+1];
  int *Temp;
  int i, kidal1, kidal2;

  Temp = new int[kanan+1];
  kidal1 = kiri;
  kidal2 = tengah+1;
  i = kiri;

  while( kidal1 <= tengah && kidal2 <= kanan){
      if (T[kidal1] <= T[kidal2]){
        Temp[i] = T[kidal1];
        kidal1++;
      } else {
        Temp[i] = T[kidal2];
        kidal2++;
      }
      i++;
  }

  while ( kidal1 <= tengah ){
    Temp[i] = T[kidal1];
    kidal1++;
    i++;
  }

  while ( kidal2 <= kanan ){
    Temp[i] = T[kidal2];
    kidal2++;
    i++;
  }

  for (i = kiri; i <= kanan; i++)
    T[i] = Temp[i];

  delete[] Temp;
}

//implementasi Merge Sort
void MergeSort(int *T, int i, int j){
  int k;
  if ( i < j ){
    k = (i+j)/2;
    MergeSort(T,i,k);
    MergeSort(T,k+1,j);
    Merge(T,i,k,j);
  }
}

int Partisi(int *T, int lo , int hi){
  int pivot,i,j;
  pivot = T[lo];
  i = lo - 1;
  j = hi + 1;
  while(true) {
    do {
      i++;
    } while (T[i] < pivot);
    do{
      j--;
    } while(T[j] > pivot);

    if ( i >= j ){
      return j;
    }
    swap(T[i],T[j]);
  }
}

//implementasi Quick Sort
void QuickSort(int *T, int i, int j){
  int k;
  if ( i < j ){
    k = Partisi(T,i,j);
    QuickSort(T,i,k);
    QuickSort(T,k+1,j);
  }
}

//validasi input banyaknya data
bool validasiN(int n){
  return (n == 1000 || n == 5000 || n == 10000 || n == 50000 || n == 100000 || n == 500000 || n == 1000000);
}

void BacaNamaFile(int n, char* in, char* out){
  if (n == 1000){
    strcpy(in,"data1000.txt");
    strcpy(out,"sort1000.txt");
  } else if (n == 5000){
    strcpy(in,"data5000.txt");
    strcpy(out,"sort5000.txt");
  } else if (n == 10000){
    strcpy(in,"data10000.txt");
    strcpy(out,"sort10000.txt");
  } else if (n == 50000){
    strcpy(in,"data50000.txt");
    strcpy(out,"sort50000.txt");
  } else if (n == 100000){
    strcpy(in,"data100000.txt");
    strcpy(out,"sort100000.txt");
  } else if (n == 500000){
    strcpy(in,"data500000.txt");
    strcpy(out,"sort500000.txt");
  } else if (n == 1000000){
    strcpy(in,"data1000000.txt");
    strcpy(out,"sort1000000.txt");
  } else {
    cout<<"Banyaknya data tidak ada pada spesifikasi"<<endl;
  }
}


int main(int argc, char** argv){

  /*KAMUS*/
  ifstream in_stream;
  ofstream out_stream;
  int i,n;
  int* TI;
  string input;
  char FILE_IN[50];
  char FILE_OUT[50];
  char jenisSort[20];

  /*ALGORITMA*/
  cout<<"Masukkan banyaknya data yang akan diurutkan:"<<endl;
  cout<<"Pilihan: \n   1000 \n   5000 \n   10000 \n   50000 \n   100000 \n   500000 \n   1000000"<<endl<<">> ";
  cin>>n;
  TI = new int[n+1];

  if (validasiN(n))
    BacaNamaFile(n,FILE_IN,FILE_OUT);
  else{
    cout<<"Banyaknya data tidak sesuai spesifikasi"<<endl;
    return -1;
  }

  //membuka file eksternal
  in_stream.open(FILE_IN);
  out_stream.open(FILE_OUT);

  //Membaca data dari file eksternal
  for (i =1; i <= n; i++){
    in_stream >> input;
    TI[i] = strToInt(input);
  }

  cout<<"Masukkan jenis sorting yang akan digunakan:"<<endl;
  cout<<"Pilihan: \n   insertion \n   selection \n   merge \n   quick"<<endl<<">> ";
  cin>>jenisSort;
  clock_t tStart = clock();
  if (strcmp(jenisSort,"insertion") == 0){
    out_stream<<"Digunakan Insertion Sort\n";
    InsertionSort(TI,n);
  } else if (strcmp(jenisSort,"selection") == 0){
    out_stream<<"Digunakan Selection Sort\n";
    SelectionSort(TI,n);
  } else if (strcmp(jenisSort,"merge") == 0){
    out_stream<<"Digunakan Merge Sort\n";
    MergeSort(TI,1,n);
  } else if (strcmp(jenisSort,"quick") == 0){
    out_stream<<"Digunakan Quick Sort\n";
    QuickSort(TI,1,n);
  } else {
    out_stream<<"Jenis Sort tidak sesuai spesifikasi"<<endl;
    return -1;
  }

  out_stream<<endl<<"Data setelah diurutkan\n";
  for (int i =1; i <= n; i++){
    out_stream<<TI[i]<<endl;
  }

  out_stream<<endl<<"Waktu yang diperlukan:" <<endl<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<" detik"<<endl;

  delete[] TI;
  //Menutup file
  in_stream.close();
  out_stream.close();

  return 0;
}
